package daemon

import (
	"reconciliationEngine/model"
	"time"
	"reconciliationEngine/utility"
	"reconciliationEngine/utility/fetcher"
	"fmt"
	"strconv"
)

type BlockchainFetcher struct {
	daemon

	config model.Configuration

	btcFetcher  *fetcher.BitcoinFetcher
	ltcFetcher  *fetcher.LitecoinFetcher
	dashFetcher *fetcher.DashFetcher

}

func NewBlockchainFetcher(configuration model.Configuration, model *model.Model, bcTxCh *chan model.Transaction, exTxCh *chan model.Transaction) *BlockchainFetcher {
	bcf := new(BlockchainFetcher)

	bcf.config = configuration
	bcf.Initialize(configuration.ReconEngine.BCfetchTimePeriod, model, bcTxCh, exTxCh)

	return bcf
}

func (this *BlockchainFetcher) Initialize(timeout int8, model *model.Model, bcTxCh *chan model.Transaction, exTxCh *chan model.Transaction)  {
	this.timeout = timeout
	this.model = model

	statusContainer, err := utility.NewTransationStatusContainer(model)
	if err != nil {
		panic(err)
	}

	this.btcFetcher  = fetcher.NewBitcoinFetcher(this.config.BlockchainAPI.BlockcypherURL, this.config.BlockchainAPI.BlockcypherApiKey, model, statusContainer, bcTxCh, exTxCh)
	this.ltcFetcher  = fetcher.NewLitecoinFetcher(this.config.BlockchainAPI.BlockcypherURL, this.config.BlockchainAPI.BlockcypherApiKey, model, statusContainer, bcTxCh, exTxCh)
	this.dashFetcher = fetcher.NewDashFetcher(this.config.BlockchainAPI.BlockcypherURL, this.config.BlockchainAPI.BlockcypherApiKey, model, statusContainer, bcTxCh, exTxCh)
}

func (this *BlockchainFetcher) DoWork() {
	for {
		unproccessedDeltixTransactions, _ := this.model.NewDeltixTransactions()

		if len(unproccessedDeltixTransactions) == 0 {
			fmt.Println("No transactions found, sleeping...")
			time.Sleep(time.Second * time.Duration(this.timeout))

			continue
		}

		for _, item := range unproccessedDeltixTransactions {
			switch item.CurrencyID {
				case "BTC":
					this.btcFetcher.Run(item)

				case "LTC":
					this.ltcFetcher.Run(item)

				case "DASH":
					this.dashFetcher.Run(item)
			}
		}

		fmt.Println(strconv.Itoa(len(unproccessedDeltixTransactions)) + " transactions have been handled, sleeping...")

		time.Sleep(time.Second * time.Duration(this.timeout))
	}
}