package daemon

import (
	"reconciliationEngine/utility"
					"reconciliationEngine/model"
		"reconciliationEngine/utility/validator"
	"fmt"
	"time"
)

type MatchingEngine struct {
	daemon

	bcCh *chan model.Transaction // channel to push blockchain transactions
	exCh *chan model.Transaction // channel to push exchange transactions
	
	exchangeTxs   []*model.Transaction
	blockchainTxs []*model.Transaction

	matchingPairs 	   [][2]model.Transaction
	matchedPairs 	   [][2]model.Transaction
	partlyMatchedPairs [][2]model.Transaction

	trValidator 	*validator.TransactionValidator
	trPairValidator *validator.TransactionPairValidator

	statusContainer *utility.TransactionStatusContainer
}

func NewMatchingEngine(timeout int8, model *model.Model, bcTxCh *chan model.Transaction, exTxCh *chan model.Transaction) *MatchingEngine {
	ME := new(MatchingEngine)

	ME.bcCh = bcTxCh
	ME.exCh = exTxCh

	ME.Initialize(timeout, model)

	return ME
}

func (this *MatchingEngine) Initialize(timeout int8, model *model.Model) {
	this.timeout = timeout
	this.model = model

	statusContainer, err := utility.NewTransationStatusContainer(model)
	if err != nil {
		panic(err)
	}

	this.trValidator = validator.NewTransactionValidator()
	this.trPairValidator = validator.NewTransactionPairValidator()
	this.statusContainer = statusContainer

	this.blockchainTxs, err = model.ReconciliableBlockchainTransactions()
	if err != nil {
		panic(err)
	}

	this.exchangeTxs, err   = model.ReconciliableDeltixTransactions()
	if err != nil {
		panic(err)
	}

	go this.collectTransactions()
}

func (this *MatchingEngine) collectTransactions() {
	for {
		select {
			case transaction := <- *this.bcCh:
				this.blockchainTxs = append(this.blockchainTxs, &transaction)

			case transaction := <- *this.exCh:
				this.exchangeTxs = append(this.exchangeTxs, &transaction)

			//default:
		}
	}
}

func (this *MatchingEngine) clearMemory() {
	this.matchingPairs = make([][2]model.Transaction, 0)
	this.matchedPairs = make([][2]model.Transaction, 0)
	this.partlyMatchedPairs = make([][2]model.Transaction, 0)
}

func (this *MatchingEngine) DoWork() {
	for {
		// Going through deltix transactions to look for pairs
		for i := 0; i < len(this.exchangeTxs); i++ {
			// Validating transactions
			validationRes := this.trValidator.Validate(*this.exchangeTxs[i])

			// If transaction is not valid, we pass it and set the comment
			if !validationRes.Valid {
				comment := "Failed validation on: "
				for _, factor := range validationRes.FailFactors {
					comment += factor + ", "
				}

				comment = comment[:len(comment) - 2]

				this.model.UpdateInvalidExchangeTransaction(this.exchangeTxs[i].ID, this.statusContainer.GetInstance("rejected").ID, comment)

				this.exchangeTxs = append(this.exchangeTxs[:i], this.exchangeTxs[i + 1:]...)
				i--
			} else {
				// If transaction is valid, we try to find a pair on the blockchain for it
				matchingTransactions := make([]*model.Transaction, 0)

				for j := 0; j < len(this.blockchainTxs); j++ {
					// If transaction hash is the same and the source is not, we add them to matching tx list
					if *this.exchangeTxs[i].TxHash == *this.blockchainTxs[j].TxHash && this.exchangeTxs[i].SourceID != this.blockchainTxs[j].SourceID {
						matchingTransactions = append(matchingTransactions, this.blockchainTxs[j])

						this.blockchainTxs = append(this.blockchainTxs[:j], this.blockchainTxs[j + 1:]...)
						j--
					}
				}

				// Removing transactions without pair from reconciliation process
				if len(matchingTransactions) == 0 {
					this.model.UpdateNotMatchedExchangeTransaction(this.exchangeTxs[i].ID, this.statusContainer.GetInstance("not_matched").ID)

					this.exchangeTxs = append(this.exchangeTxs[:i], this.exchangeTxs[i + 1:]...)
					i--

					continue
				}

				// Adding matching transactions to the result array
				for _, matchingTransaction := range matchingTransactions {
					var pair = [2]model.Transaction{*this.exchangeTxs[i], *matchingTransaction}

					this.matchingPairs = append(this.matchingPairs, pair)
				}

				this.exchangeTxs = append(this.exchangeTxs[:i], this.exchangeTxs[i + 1:]...)
				i--
			}
		}

		// Going through matching pairs to validate and match the pairs
		for _, pair := range this.matchingPairs {
			validationRes := this.trPairValidator.Validate(pair)

			// Adding fully matched pairs to the final array
			if validationRes.Valid {
				this.matchedPairs = append(this.matchedPairs, pair)
			} else {
				// If validation is failed, we look how much fields are mismatched and adding to partly matched array with mismatching comment
				factors := validationRes.FailFactors

				if len(factors) != 0 && len(factors) < validator.TRANSACTION_PAIR_VALIDATION_FACTORS {
					this.partlyMatchedPairs = append(this.partlyMatchedPairs, pair)
				}
			}
		}

		// Inserting matched transactions
		for _, pair := range this.matchedPairs {
			this.model.UpdateMatchedTransaction(pair[0].ID,this.statusContainer.GetInstance("matched").ID)
			this.model.UpdateMatchedTransaction(pair[1].ID,this.statusContainer.GetInstance("matched").ID)
		}

		// Checking if partly matched deltix transactions have matched duplicate
		for _, pair := range this.partlyMatchedPairs {

			matchedDuplicates := make([][2]model.Transaction, 0)

			for _, matchedPair := range this.matchedPairs {
				if matchedPair[0].ID == pair[0].ID {
					matchedDuplicates = append(matchedDuplicates, matchedPair)
				}
			}

			// If deltix transaction doesn't have matched duplicate, we mark it as partly matched
			if len(matchedDuplicates) == 0 {
				factors := this.trPairValidator.Validate(pair).FailFactors

				if len(factors) != 0 && len(factors) < validator.TRANSACTION_PAIR_VALIDATION_FACTORS {
					comment := ""
					commentTemplate := "%s mismatch"

					for _, factor := range factors {
						comment += factor + ", "
					}

					comment = comment[:len(comment) - 2]
					comment = fmt.Sprintf(commentTemplate, comment)

					this.model.UpdateInvalidTransaction(pair[0].ID, this.statusContainer.GetInstance("partly_matched").ID, comment)
					this.model.UpdateInvalidTransaction(pair[1].ID, this.statusContainer.GetInstance("partly_matched").ID, comment)
				}
			} else {
				// Otherwise, we mark it as not matched one
				this.model.UpdateNotMatchedTransaction(pair[1].ID, this.statusContainer.GetInstance("not_matched").ID)
			}
		}

		this.clearMemory()
		time.Sleep(time.Second * time.Duration(this.timeout))
	}
}