package daemon

import "reconciliationEngine/model"

type backgroundProcess interface {
	Initialize(timeout int8, model *model.Model)
	DoWork()
}

type daemon struct {
	timeout int8
	model *model.Model

	backgroundProcess
}