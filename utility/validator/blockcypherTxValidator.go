package validator

import "reconciliationEngine/model/jsonResponse"


type BlockcypherTxValidator struct {
	validator
}

func NewBlockcypherTxValidator() *BlockcypherTxValidator {
	validator := new(BlockcypherTxValidator)

	return validator
}

func (this *BlockcypherTxValidator) Validate(response jsonResponse.BlockcypherRawTx) (bool)  {
	if len(response.Outputs) == 0 {
		return false
	}

	return true
}