package validator

type validator interface {
	Validate(resource interface{}) (bool)
}
