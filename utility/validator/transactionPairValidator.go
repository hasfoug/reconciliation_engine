package validator

import (
	"reconciliationEngine/model"
	"strings"
)

const TRANSACTION_PAIR_VALIDATION_FACTORS = 3

type TransactionPairValidator struct {
	*model.ValidationResult

	validator
}

func NewTransactionPairValidator() *TransactionPairValidator {
	trValidator := new(TransactionPairValidator)


	return trValidator
}

func (this TransactionPairValidator) Validate(pair [2]model.Transaction) *model.ValidationResult {
	this.ValidationResult = model.NewValidationResult()

	exchangeTr := pair[0]
	blockchainTr := pair[1]

	if exchangeTr.Amount != blockchainTr.Amount {
		this.AddFailFactor("Amount")
	}

	if strings.TrimSpace(exchangeTr.SrcAddr) != strings.TrimSpace(blockchainTr.SrcAddr) {
		this.AddFailFactor("Source address")
	}

	if strings.TrimSpace(exchangeTr.DestAddr) != strings.TrimSpace(blockchainTr.DestAddr) {
		this.AddFailFactor("Destination address")
	}

	if len(this.FailFactors) > 0 {
		this.Valid = false
	}

	return this.ValidationResult
}