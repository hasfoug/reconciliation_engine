package validator

import (
	"reconciliationEngine/model"
			)

type TransactionValidator struct {
	*model.ValidationResult

	validator
}

func NewTransactionValidator() *TransactionValidator {
	trValidator := new(TransactionValidator)

	return trValidator
}

func (this *TransactionValidator) Validate(transaction model.Transaction) *model.ValidationResult {
	this.ValidationResult = model.NewValidationResult()
	/*
	if transaction.TxType == nil {
		this.AddFailFactor("txType")
	}

	if transaction.TxHash == nil {
		this.AddFailFactor("txHash")
	}

	switch transaction.SourceID {
		case model.DeltixSource:
			if transaction.TxID == nil {
				this.AddFailFactor("txId")
			}
		default:
			if transaction.Fee  == nil {
				this.AddFailFactor("fee")
			}

			if transaction.OriginalStatusID == nil {
				this.AddFailFactor("originalStatus")
			}
	}
*/
	if len(this.FailFactors) > 0 {
		this.Valid = false
	}

	return this.ValidationResult
}