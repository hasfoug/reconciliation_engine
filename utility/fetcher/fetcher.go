package fetcher

import (
	"reconciliationEngine/model"
	"reconciliationEngine/utility"
	)

type fetcher struct {
	model *model.Model
	statusContainer *utility.TransactionStatusContainer
	
	bcCh *chan model.Transaction // channel to push blockchain transactions
	exCh *chan model.Transaction // channel to push exchange transactions
}

func (this *fetcher) pushBcTransaction(transaction model.Transaction)  {
	*this.bcCh <- transaction
}

func (this *fetcher) pushExTransaction(transaction model.Transaction)  {
	*this.exCh <- transaction
}