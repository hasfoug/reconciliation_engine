package fetcher

import (
		"reconciliationEngine/model"
	"reconciliationEngine/model/jsonResponse"
	"reconciliationEngine/utility"
	"reconciliationEngine/utility/validator"
	"log"
	"reconciliationEngine/utility/apiClient"
)

type LitecoinFetcher struct {
	Client *apiClient.BlockcypherClient
	validator *validator.BlockcypherTxValidator

	fetcher
}

func NewLitecoinFetcher(baseUrl string, apiKey string, model *model.Model, statusContainer *utility.TransactionStatusContainer, bcTxCh *chan model.Transaction, exTxCh *chan model.Transaction) *LitecoinFetcher {
	fetcher := new(LitecoinFetcher)

	fetcher.model = model
	fetcher.Client = apiClient.NewBlockcypherClient(baseUrl, apiKey)
	fetcher.validator = validator.NewBlockcypherTxValidator()
	fetcher.statusContainer = statusContainer

	fetcher.bcCh = bcTxCh
	fetcher.exCh = exTxCh

	return fetcher
}

func (this *LitecoinFetcher) Run(transaction *model.Transaction) error {
	bcTransaction, err := this.fetch(transaction)
	if err != nil {
		return err
	}

	postActionRes := this.postAction(*bcTransaction)
	if postActionRes {
		res := this.insert(transaction, *bcTransaction)
		if !res {
			panic("Check if database is configured")
		}
	}

	return nil
}

func (this *LitecoinFetcher) fetch (transaction *model.Transaction) (*jsonResponse.BlockcypherRawTx, error)  {
	return this.Client.GetRawTx(transaction.CurrencyID, *transaction.TxHash)
}

func (this *LitecoinFetcher) postAction(response jsonResponse.BlockcypherRawTx) bool {
	return this.validator.Validate(response)
}

func (this *LitecoinFetcher) insert(exchangeTransaction *model.Transaction, blockchainTransaction jsonResponse.BlockcypherRawTx) bool {
	status := this.statusContainer.GetInstance("reconciliable")
	if status == nil {
		log.Fatal("DB is not configured: reconciliable status missing")

		return false
	}

	err := this.model.UpdateExchangeTransaction(exchangeTransaction.ID, status.ID)
	if err != nil {
		panic(err)
	}

	exchangeTransaction.StatusID = &status.ID
	this.pushExTransaction(*exchangeTransaction)

	for _, item := range blockchainTransaction.Outputs {
		transaction := model.Transaction{CurrencyID:exchangeTransaction.CurrencyID, StatusID:&status.ID, Amount:utility.ConvertSatoshi(item.Value), SrcAddr:exchangeTransaction.SrcAddr, DestAddr:item.Addresses[0], TxHash:&blockchainTransaction.Hash, SourceID:model.LitecoinSource}
		Id, err := this.model.NewBlockchainTransaction(transaction)
		if err != nil {
			panic(err)
		}

		transaction.ID = Id
		this.pushBcTransaction(transaction)
	}

	return true
}