package utility

import (
	"net/http"
	"encoding/json"
	"time"
	)

type NetClient struct {
	Client *http.Client
}

func NewNetClient() *NetClient {
	netClient := new (NetClient)
	netClient.Client = &http.Client{
		Timeout: time.Second * 10,
	}

	return netClient
}

func (netClient *NetClient) GetJson(url string, target interface{}) error {
	r, err := netClient.Client.Get(url)
	if err != nil {
		return err
	}

	defer r.Body.Close()

	return json.NewDecoder(r.Body).Decode(target)
}
