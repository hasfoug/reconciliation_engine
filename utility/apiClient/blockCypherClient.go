package apiClient

import (
				"reconciliationEngine/utility"
	"reconciliationEngine/model/jsonResponse"
	"fmt"
	"strings"
)

type BlockcypherClient struct {
	baseUrl string
	apiKey  string

	*utility.NetClient
}

func NewBlockcypherClient(baseUrl string, apiKey string) *BlockcypherClient {
	client := new (BlockcypherClient)

	client.baseUrl = baseUrl
	client.apiKey  = apiKey
	client.NetClient = utility.NewNetClient()

	return client
}

func (this *BlockcypherClient) GetRawTx(currency string, hash string) (*jsonResponse.BlockcypherRawTx, error) {
	var response jsonResponse.BlockcypherRawTx

	err := this.NetClient.GetJson(fmt.Sprintf("%s/v1/%s/main/txs/%s", this.baseUrl, strings.ToLower(currency), hash), &response)
	if err != nil {
		return nil, err
	}

	return &response, nil
}