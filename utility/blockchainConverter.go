package utility

func ConvertSatoshi(satoshi int64) float64 {
	return float64(satoshi) / 100000000
}
