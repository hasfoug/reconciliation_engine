package utility

import "reconciliationEngine/model"

type TransactionStatusContainer struct {
	m *model.Model
	container map[string] *model.TransactionStatus
}

func NewTransationStatusContainer(m *model.Model) (*TransactionStatusContainer, error) {
	transactionStatusContainer := new(TransactionStatusContainer)

	transactionStatusContainer.m = m
	transactionStatusContainer.container = make(map[string]*model.TransactionStatus)

	err := transactionStatusContainer.loadData()
	if err != nil {
		return nil, err
	}

	return transactionStatusContainer, nil
}

func (this *TransactionStatusContainer) loadData() error {
	statuses, err := this.m.TransactionStatuses()
	if err != nil {
		return err
	}

	for _, item := range statuses {
		this.container[item.Value] = item
	}

	return nil
}

func (this *TransactionStatusContainer) GetInstance(id string) *model.TransactionStatus {
	instance := this.container[id]

	return instance
}