package main

import (
	"github.com/BurntSushi/toml"
	"reconciliationEngine/model"
	"reconciliationEngine/daemon"
	"log"
	"reconciliationEngine/database"
	"time"
	"runtime"
	"os"
	"go/build"
	)

func main() {
	projDir := build.Default.GOPATH + "/src/reconciliationEngine"

	logFile, err := os.OpenFile(projDir + "/var/log", os.O_RDWR | os.O_CREATE | os.O_APPEND, 0666)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}
	defer logFile.Close()
	log.SetOutput(logFile)

	var config model.Configuration
	if _, err := toml.DecodeFile(projDir + "/config/config.toml", &config); err != nil {
		log.Fatal(err)
		return
	}

	db, err := database.InitDB(config)
	if err != nil {
		log.Println(err)
	}

	log.Println("Reconciliation engine started at " + time.Now().String())

	m := model.New(db)

	BcfMeBlockchainTxChan := make(chan model.Transaction)
	BcfMeExchangeTxChan   := make(chan model.Transaction)

	bcFetcher := daemon.NewBlockchainFetcher(config, m, &BcfMeBlockchainTxChan, &BcfMeExchangeTxChan)
	matchingEngine := daemon.NewMatchingEngine(config.ReconEngine.MEfetchTimePeriod, m, &BcfMeBlockchainTxChan, &BcfMeExchangeTxChan)

	go bcFetcher.DoWork()
	go matchingEngine.DoWork()

	runtime.Goexit()
}
