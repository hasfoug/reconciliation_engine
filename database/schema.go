package database

type Schema []string

var schema = Schema {
	"CREATE TABLE `transactions` ( `id` INT UNSIGNED NOT NULL AUTO_INCREMENT, `currency_id` VARCHAR(10) NOT NULL , `tx_id` VARCHAR(25) NULL , `tx_type_id` INT NULL , `amount` DECIMAL(40,20) NOT NULL ,  `src_address` VARCHAR(255) NOT NULL , `dest_address` VARCHAR(255) NOT NULL , `tx_hash` VARCHAR(255) NULL , `account_id` VARCHAR(25) NULL , `status_id` INT NOT NULL , `original_status_id` INT NULL , `insert_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, `last_update` DATETIME NULL , `fee` DECIMAL(20,10) NULL , `source_id` INT NOT NULL , `comment` TEXT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB",
	"CREATE TABLE `currencies` ( `id` VARCHAR(10) NOT NULL, `full_name` VARCHAR(20) NOT NULL , `api_base_url` VARCHAR(50) NOT NULL, `api_key` VARCHAR(50) NULL, PRIMARY KEY (`id`)) ENGINE = InnoDB;",
	"CREATE TABLE `transaction_statuses` ( `id` INT NOT NULL AUTO_INCREMENT , `value` VARCHAR(25) NOT NULL , `original` BOOLEAN NOT NULL , `mapped_status` INT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;",

	"ALTER TABLE `transactions` ADD FOREIGN KEY (`currency_id`) REFERENCES `currencies`(`id`) ON DELETE CASCADE ON UPDATE CASCADE ;",
	"ALTER TABLE `transactions` ADD FOREIGN KEY (`status_id`) REFERENCES `transaction_statuses`(`id`) ON DELETE CASCADE ON UPDATE CASCADE ;",
	"ALTER TABLE `transactions` ADD FOREIGN KEY (`original_status_id`) REFERENCES `transaction_statuses`(`id`) ON DELETE CASCADE ON UPDATE CASCADE ;",
}
