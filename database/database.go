package database

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"reconciliationEngine/model"
	"fmt"
)

type sqlDB struct {
	dbConn *sql.DB

	sqlSelectNewDeltixTransactions *sql.Stmt
	sqlSelectReconciliableDeltixTransactions *sql.Stmt
	sqlSelectReconciliablBlockhainTransactions *sql.Stmt
	sqlSelectTransactionStatuses *sql.Stmt

	sqlUpdateExchangeTransaction *sql.Stmt
	sqlUpdateInvalidExchangeTransaction *sql.Stmt
	sqlUpdateNotMatchedExchangeTransaction *sql.Stmt
	sqlUpdateMatchedExchangeTransaction *sql.Stmt

	sqlInsertBlockchainTransaction *sql.Stmt
}

func InitDB (config model.Configuration) (*sqlDB, error) {
	if dbConn, err := sql.Open(config.Database.Driver,  fmt.Sprintf("%s:%s@/%s", config.Database.User, config.Database.Password, config.Database.Name)); err != nil {
		return nil, err
	} else {
		p := &sqlDB{dbConn: dbConn}

		if err := p.dbConn.Ping(); err != nil {
			return nil, err
		}

		if err := p.migrate(); err != nil {
			return nil, err
		}

		if err := p.prepareSqlStatements(); err != nil {
			return nil, err
		}

		return p, nil
	}
}

func (p *sqlDB) migrate() error {
	for i := range schema {
		p.dbConn.Query(schema[i])
	}

	return nil
}

func (p *sqlDB) prepareSqlStatements() (err error) {
	if p.sqlSelectNewDeltixTransactions, err = p.dbConn.Prepare(
		fmt.Sprintf("SELECT tr.id, tr.tx_id, tr.currency_id, tr.amount, tr.src_address, tr.dest_address, tr.tx_type_id, tr.tx_hash, tr.source_id FROM transactions tr WHERE tr.status_id IN (SELECT id FROM transaction_statuses ts WHERE ts.value IN ('new'))  AND tr.source_id = %d", model.DeltixSource),
	); err != nil {
		return err
	}

	if p.sqlSelectReconciliableDeltixTransactions, err = p.dbConn.Prepare(
		fmt.Sprintf("SELECT tr.id, tr.tx_id, tr.currency_id, tr.amount, tr.src_address, tr.dest_address, tr.tx_type_id, tr.tx_hash, tr.source_id FROM transactions tr WHERE tr.status_id IN (SELECT id FROM transaction_statuses ts WHERE ts.value IN ('reconciliable', 'not_matched'))  AND tr.source_id = %d", model.DeltixSource),
	); err != nil {
		return err
	}

	if p.sqlSelectReconciliablBlockhainTransactions, err = p.dbConn.Prepare(
		fmt.Sprintf("SELECT tr.id, tr.currency_id, tr.amount, tr.src_address, tr.dest_address, tr.tx_type_id, tr.tx_hash, tr.source_id, tr.fee, tr.original_status_id FROM transactions tr WHERE tr.status_id IN (SELECT id FROM transaction_statuses ts WHERE ts.value IN ('reconciliable', 'not_matched'))  AND tr.source_id != %d", model.DeltixSource),
	); err != nil {
		return err
	}

	if p.sqlSelectTransactionStatuses, err = p.dbConn.Prepare(
		"SELECT ts.id, ts.value, ts.original, ts.mapped_status FROM transaction_statuses ts",
	); err != nil {
		return err
	}

	if p.sqlUpdateExchangeTransaction, err = p.dbConn.Prepare(
		"UPDATE transactions SET status_id=? WHERE id=?",
	); err != nil {
		return err
	}

	if p.sqlUpdateInvalidExchangeTransaction, err = p.dbConn.Prepare(
		"UPDATE transactions SET status_id=?, comment=? WHERE id=?",
	); err != nil {
		return err
	}

	if p.sqlUpdateNotMatchedExchangeTransaction, err = p.dbConn.Prepare(
		"UPDATE transactions SET status_id=?, last_update=now() WHERE id=?",
	); err != nil {
		return err
	}

	if p.sqlUpdateMatchedExchangeTransaction, err = p.dbConn.Prepare(
		"UPDATE transactions SET status_id=? WHERE id=?",
	); err != nil {
		return err
	}

	if p.sqlInsertBlockchainTransaction, err = p.dbConn.Prepare(
		"INSERT INTO transactions(currency_id, status_id, amount, src_address, dest_address, tx_hash, source_id) VALUES (?,?,?,?,?,?,?)",
	); err != nil {
		return err
	}

	return nil
}

func (p *sqlDB) SelectNewDeltixTransactions() ([]*model.Transaction, error) {
	transactions := make([]*model.Transaction, 0)

	rows, err := p.sqlSelectNewDeltixTransactions.Query()
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var transaction = model.Transaction{}

		err := rows.Scan(&transaction.ID, &transaction.TxID, &transaction.CurrencyID, &transaction.Amount, &transaction.SrcAddr, &transaction.DestAddr, &transaction.TxType, &transaction.TxHash, &transaction.SourceID)
		if err != nil {
			return nil, err
		}

		transactions = append(transactions, &transaction)
	}

	return transactions, nil
}

func (p *sqlDB) SelectReconciliableDeltixTransactions() ([]*model.Transaction, error) {
	transactions := make([]*model.Transaction, 0)

	rows, err := p.sqlSelectReconciliableDeltixTransactions.Query()
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var transaction = model.Transaction{}

		err := rows.Scan(&transaction.ID, &transaction.TxID, &transaction.CurrencyID, &transaction.Amount, &transaction.SrcAddr, &transaction.DestAddr, &transaction.TxType, &transaction.TxHash, &transaction.SourceID)
		if err != nil {
			return nil, err
		}

		transactions = append(transactions, &transaction)
	}

	return transactions, nil
}

func (p *sqlDB) SelectReconciliableBlockchainTransactions() ([]*model.Transaction, error) {
	transactions := make([]*model.Transaction, 0)

	rows, err := p.sqlSelectReconciliablBlockhainTransactions.Query()
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var transaction = model.Transaction{}

		err := rows.Scan(&transaction.ID, &transaction.CurrencyID, &transaction.Amount, &transaction.SrcAddr, &transaction.DestAddr, &transaction.TxType, &transaction.TxHash, &transaction.SourceID, &transaction.Fee, &transaction.OriginalStatusID)
		if err != nil {
			return nil, err
		}

		transactions = append(transactions, &transaction)
	}

	return transactions, nil
}

func (p *sqlDB) SelectTransactionStatuses() ([]*model.TransactionStatus, error) {
	statuses := make([]*model.TransactionStatus, 0)

	rows, err := p.sqlSelectTransactionStatuses.Query()
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var status = model.TransactionStatus{}

		err := rows.Scan(&status.ID, &status.Value, &status.Original, &status.MappedStatus)
		if err != nil {
			return nil, err
		}

		statuses = append(statuses, &status)
	}

	return statuses, nil
}

func (p *sqlDB) UpdateExchangeTransaction(transactionId int64, statusId int8) error {
	_, err := p.sqlUpdateExchangeTransaction.Exec(statusId, transactionId)
	if err != nil {
		return err
	}

	return nil
}

func (p *sqlDB) UpdateInvalidExchangeTransaction(transactionId int64, statusId int8, comment string) error {
	_, err := p.sqlUpdateInvalidExchangeTransaction.Exec(statusId, comment, transactionId)
	if err != nil {
		return err
	}

	return nil
}

func (p *sqlDB) UpdateNotMatchedExchangeTransaction(transactionId int64, statusId int8) error {
	_, err := p.sqlUpdateNotMatchedExchangeTransaction.Exec(statusId, transactionId)
	if err != nil {
		return err
	}

	return nil
}

func (p *sqlDB) UpdateMatchedExchangeTransaction(transactionId int64, statusId int8) error {
	_, err := p.sqlUpdateMatchedExchangeTransaction.Exec(statusId, transactionId)
	if err != nil {
		return err
	}

	return nil
}

func (p *sqlDB) InsertBlockchainTransaction(currencyId string, statusId int8, amount float64, srcAddress string, destAddress string, txHash string, sourceId int8) (int64, error) {
	res, err := p.sqlInsertBlockchainTransaction.Exec(currencyId, statusId, amount, srcAddress, destAddress, txHash, sourceId)
	if err != nil {
		return 0,err
	}

	ID, err := res.LastInsertId()
	if err != nil {
		return 0,err
	}

	return ID, nil
}