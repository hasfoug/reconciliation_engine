package model

type ValidationResult struct {
	Valid bool
	FailFactors []string
}

func NewValidationResult() *ValidationResult {
	result := new(ValidationResult)

	result.Valid = true
	result.FailFactors = make([]string, 0)

	return result
}

func (this *ValidationResult) AddFailFactor(factor string)  {
	this.FailFactors = append(this.FailFactors, factor)
}