package model

// Transaction struct (Model)
/*
type Transaction struct {
	ID 	       int64     		 `json:"id"`
	Currency   Currency  		 `json:"currency"`
	TxId       string    		 `json:"txId"`
	TxType     int8 	 		 `json:"type"`
	Amount	   float64   		 `json:"amount"`
	SrcAddr	   string    		 `json:"srcAddress"`
	DestAddr   string    		 `json:"destAddress"`
	TxHash     string    		 `json:"hash"`
	UserId     string    		 `json:"userId"`
	Status     TransactionStatus `json:"status"`
	OrigStatus TransactionStatus `json:"origStatus"`
	InsertTime time.Time 		 `json:"createdAt"`
	LastUpdate time.Time 		 `json:"updatedAt"`
	Fee		   float64	 		 `json:"fee"`
	Source     int8      		 `json:"source"`
	Comment    string    		 `json:"comment"`
}
*/
/*
type NewExchangeTransaction struct {
	ID 	  			int64
	CurrencyID 		string
	CurrencyBaseUrl string
	SrcAddr			string
	TxHash 			string
}
*/
type Transaction struct {
	ID 	  			 int64
	TxID			 *string
	CurrencyID 		 string
	Amount 			 float64
	SrcAddr			 string
	DestAddr		 string
	TxType			 *int8
	TxHash 			 *string
	StatusID		 *int8
	SourceID		 int8
	Fee				 *float64
	OriginalStatusID *int8
}

func (m *Model) NewDeltixTransactions() ([]*Transaction, error)  {
	return m.SelectNewDeltixTransactions()
}

func (m *Model) ReconciliableDeltixTransactions() ([]*Transaction, error)  {
	return m.SelectReconciliableDeltixTransactions()
}

func (m *Model) ReconciliableBlockchainTransactions() ([]*Transaction, error)  {
	return m.SelectReconciliableBlockchainTransactions()
}

func (m *Model) UpdateTransactionStatus(transactionId int64, statusId int8) error {
	return m.UpdateExchangeTransaction(transactionId, statusId)
}

func (m *Model) UpdateInvalidTransaction(transactionId int64, statusId int8, comment string) error {
	return m.UpdateInvalidExchangeTransaction(transactionId, statusId, comment)
}

func (m *Model) UpdateNotMatchedTransaction(transactionId int64, statusId int8) error {
	return m.UpdateNotMatchedExchangeTransaction(transactionId, statusId)
}

func (m *Model) UpdateMatchedTransaction(transactionId int64, statusId int8) error {
	return m.UpdateMatchedExchangeTransaction(transactionId, statusId)
}

func (m *Model) NewBlockchainTransaction(transaction Transaction) (int64, error) {
	return m.InsertBlockchainTransaction(transaction.CurrencyID, *transaction.StatusID, transaction.Amount, transaction.SrcAddr, transaction.DestAddr, *transaction.TxHash, transaction.SourceID)
}