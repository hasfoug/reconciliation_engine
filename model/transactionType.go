package model

const (
	WithdrawalType 	int8 = 1
	DepositType	 	int8 = 2
	CommissionType 	int8 = 3
)
