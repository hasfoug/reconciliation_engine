package jsonResponse

type BlockcypherRawTx struct {
	Hash 	string 					 `json:"hash"`
	Fee 	int64 				 	 `json:"fees"`
	Inputs  []blockcypherRawTxInput  `json:"inputs"`
	Outputs []blockcypherRawTxOutput `json:"outputs"`
}

type blockcypherRawTxInput struct {
	Value 	  int64    `json:"output_value"`
	Addresses []string `json:"addresses"`
}

type blockcypherRawTxOutput struct {
	Value 	  int64    `json:"value"`
	Addresses []string `json:"addresses"`
}