package model

type db interface {
	SelectNewDeltixTransactions() ([]*Transaction, error)
	SelectReconciliableDeltixTransactions() ([]*Transaction, error)
	SelectReconciliableBlockchainTransactions() ([]*Transaction, error)
	SelectTransactionStatuses() ([]*TransactionStatus, error)

	UpdateExchangeTransaction(transactionId int64, statusId int8) error
	UpdateInvalidExchangeTransaction(transactionId int64, statusId int8, comment string) error
	UpdateNotMatchedExchangeTransaction(transactionId int64, statusId int8) error
	UpdateMatchedExchangeTransaction(transactionId int64, statusId int8) error

	InsertBlockchainTransaction(currencyId string, statusId int8, amount float64, srcAddress string, destAddress string, txHash string, sourceId int8) (int64, error)
}

type Model struct {
	db
}

func New(db db) *Model {
	return &Model{
		db: db,
	}
}

