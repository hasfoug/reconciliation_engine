package model

const (
	DeltixSource 	  int8 = 1
	BitcoinSource	  int8 = 2
	EthereumSource 	  int8 = 3
	LitecoinSource 	  int8 = 4
	DashSource 		  int8 = 5
	IotaSource 		  int8 = 6
	BitcoinGoldSource int8 = 7
	RippleSource 	  int8 = 8
)
