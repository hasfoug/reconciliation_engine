package model

// Currency struct (Model)
type Currency struct {
	ID 	       string    `json:"id"`
	Name       string    `json:"name"`
	BaseUrl    string    `json:"baseUrl"`
	ApiKey     string 	 `json:"apiKey"`
}