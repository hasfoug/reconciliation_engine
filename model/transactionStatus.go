package model

type TransactionStatus struct {
	ID 			 int8
	Value 		 string
	Original 	 bool
	MappedStatus *int8
}

func (m *Model) TransactionStatuses() ([]*TransactionStatus, error)  {
	return m.SelectTransactionStatuses()
}