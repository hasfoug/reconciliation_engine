package model

type Configuration struct {
	Database 	  dbConfig
	ReconEngine   reconConfig
	BlockchainAPI blockchainAPI
}

type dbConfig struct {
	Host 	   string
	Port 	   string
	Driver     string
	User 	   string
	Password   string
	Name 	   string
}

type reconConfig struct {
	DeltixURL         string
	DeltixUsername    string
	DeltixPassword    string

	DfetchTimePeriod  int8
	BCfetchTimePeriod int8
	MEfetchTimePeriod int8
}

type blockchainAPI struct {
	BlockcypherURL 	  string
	BlockcypherApiKey string
}
